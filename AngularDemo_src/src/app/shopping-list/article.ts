
export class Article {
	public name: string;
	public price: number;
	public number_of_articles: number;
	public article_costs: number;

	constructor(name: string, price: number, number_of_articles: number) {
		this.name = name;
		this.price = price;
		this.number_of_articles =  number_of_articles;

		this.article_costs = price * number_of_articles;
	}

}
