import { Article } from './article';

export class ShoppingList {


	public shopping_list = new Array();
    // private shopping_list: Array<Article> = new Array<Article>();  // Dann müssen getter gesetzt werden
    public totalPrice = "";

	public addArticle(name: string, price: number, number_of_articles: number) {

		let article = new Article(name, price, number_of_articles);

        this.shopping_list.push(article);
        
        this.totalPrice = this.sumTotalPrice().toFixed(2);
    }

    public removeArticle(name: string, price: number, number_of_articles: number) {

		let article = new Article(name, price, number_of_articles);

        for(let index = 0; index < this.shopping_list.length; index++)
        {
            if(this.shopping_list[index] == article)
            {
                this.shopping_list.splice(index, 1);
            }
        }

        this.totalPrice = this.sumTotalPrice().toFixed(2);
    }
    
    private sumTotalPrice()
    {
        let internal_price = 0;

        for (let single_article of this.shopping_list) {
			internal_price += single_article.article_costs;
        }
        
        return internal_price;
    }

	public printResult() {
		console.log("\r\nArticle\tPrice\tNumber\tSum\r\n");
		let total_costs = 0;
		for (let single_article of this.shopping_list) {
			total_costs += single_article.article_costs;
			console.log(single_article.name 
						+ "\t" + single_article.price 
						+ "€\t" + single_article.number_of_articles 
						+ "\t" +  single_article.article_costs.toFixed(2) + "€");
		}
		console.log("\r\n\t\t\tTotal: " + total_costs.toFixed(2) + "€");
	}
}
