<?xml-stylesheet type="text/xsl" href="todo.xsl"?>
<todo name="Meine tägliche Todo-Liste">
	<item status="offen" titel="Schlafen" prio="2">Nach Hause gehen und schlafen legen</item>
	<item status="offen" titel="Arbeit" prio="1">Zur Arbeit fahren und arbeiten</item>
	<item status="angefangen" titel="Zähne putzen" prio="1">Ins Bad gehen und Zähne putzen</item>
	<item status="erledigt" titel="Aufstehen" prio="3">Munter werden und aufstehen</item>
	<item status="verspätet" titel="Frühstück" prio="3">Frühstück bereiten und essen</item>
</todo>