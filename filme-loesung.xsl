<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<?xml-stylesheet type="text/xsl" href="filme.xsl"?>
	
	<xsl:output method="html" indent="yes"/>

	<xsl:template match="/">
			
		<html>
			<body>
				<h1>Filme</h1>
				<table> 
                    <tr>
                        <th>Cover</th>
                        <th>Titel</th>
                        <th>Publication</th>
                        <th>Abstract</th>
                    </tr> 
                    
                    <xsl:apply-templates select="//film"/>
                </table>
			</body>
		</html>
		
	</xsl:template>
	<xsl:template match="film">
		<xsl:if test="@publication &lt; 2000">
			<tr>
                <th>
                    <img>
                        <xsl:attribute name="src"><xsl:value-of select="cover"/></xsl:attribute>
                    </img>
                </th>
                <th><xsl:value-of select="title"/></th>
                <th><xsl:value-of select="@publication"/></th>
                <th><xsl:value-of select="abstract"/></th>
            </tr> 
			
			
		</xsl:if>
	</xsl:template>
		
</xsl:stylesheet>