import { Component, OnInit } from '@angular/core';
import { ShoppingList } from './shopping-list';

@Component({
  selector: 'shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.scss']
})
export class ShoppingListComponent implements OnInit {

  constructor() {
    this.fillShoppingList();
  }


  ngOnInit() {
  }

  public shopping_list = new ShoppingList();
  public newArticleName = '';
  public newArticlePrice = 0;
  public newArticleAmount = 0;

  public fillShoppingList() {
    this.shopping_list.addArticle("Milch", 1.21, 3);
    this.shopping_list.addArticle("Eier", 0.38, 9);
    this.shopping_list.addArticle("Butter", 1.42, 4);
    this.shopping_list.addArticle("Mehl", 2.38, 2);
  }

  public addToList() {
    if (this.newArticleName != '' && this.newArticlePrice != 0 && this.newArticleAmount != 0) {
      this.shopping_list.addArticle(this.newArticleName, this.newArticlePrice, this.newArticleAmount);
    }
    this.newArticleName = '';
    this.newArticlePrice = 0;
    this.newArticleAmount = 0;
  }

}
