import { Component, OnInit } from '@angular/core';
import { Person } from "./person";

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {

public message: string = "";
public showDiv: boolean = false;

public messageInputBox: string;

public persons: Array<Person> = [
 new Person("Herbert", 32),
 new Person("Susi", 16),
 new Person("Angela", 24),
 new Person("Sorglos", 53)

];


constructor() { }

ngOnInit() {
}


public switchMessage(): void{
  
  this.showDiv = true;
}

}
