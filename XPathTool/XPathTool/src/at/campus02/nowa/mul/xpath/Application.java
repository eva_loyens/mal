package at.campus02.nowa.mul.xpath;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Application {

	public static void main(String[] args) {
		File input = new File(args[0]);
		if (!input.canRead()) {
			System.out.println("Konnte Datei " + input.getAbsolutePath() + "nicht lesen!");
			return;
		}
		InputStream in = null;
		try {
			in = new FileInputStream(input);
		} catch (FileNotFoundException e) {
			return;
		}
		DocumentBuilder builder = null;
		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			return;
		}
		Document doc = null;
		try {
			doc = builder.parse(in);
		} catch (SAXException | IOException e) {
			e.printStackTrace();
			return;
		}
		XPath xpath = XPathFactory.newInstance().newXPath();
		String line = null;
		Scanner scanner = new Scanner(System.in);
		while ((line = scanner.nextLine()) != null) {
			try {
				XPathExpression expr = xpath.compile(line);
				if (line.startsWith("sum(") || line.startsWith("count(")) {
					Double nl = (Double) expr.evaluate(doc, XPathConstants.NUMBER);
					System.out.println(nl);
				} else {
					NodeList nl = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
					System.out.println("Found " + nl.getLength() + " results");
					for (int i = 0; i < nl.getLength(); i++) {
						Node n = nl.item(i);
						System.out.println(n.getNodeName() + ": " + n.getNodeValue());
					}
				}
			} catch (XPathExpressionException e) {
				e.printStackTrace();
				continue;
			}
		}
		scanner.close();

	}

}
