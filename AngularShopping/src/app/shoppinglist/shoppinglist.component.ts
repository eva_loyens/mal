import { Component, OnInit } from '@angular/core';
import { Article } from './article';

@Component({
  selector: 'app-shoppinglist',
  templateUrl: './shoppinglist.component.html',
  styleUrls: ['./shoppinglist.component.css']
})
export class ShoppinglistComponent implements OnInit {

  private articles: Array<Article> = [];

  public newName: string;
  public newPrice: number;
  public newAmount: number;
  public totalPrice: number = 0;
  public selectedArticle: Article;
  

  constructor() { }

  ngOnInit() { }

  addArticle(): void {
    var article = new Article(this.newName, this.newPrice, this.newAmount);
    this.articles.push(article);
    this.totalPrice += article.sum;
  }

  removeArticle(articleToDelete: Article): void {
    //filter gibt ein Array zurück
    var newArticles = this.articles.filter(function(a: Article) {
      return a != articleToDelete;
    });
    
    // this.totalPrice -= this.articles.sum;
    this.articles = newArticles;

    if(articleToDelete == this.selectedArticle)
      this.selectedArticle = null;
  }

  selectArticle(article: Article): void {
    this.selectedArticle = article;
  }
  
  unSelect():void {
    this.selectedArticle = null;
  }

  public isActive: boolean = true;
  public isSelected: boolean = false;

  toggleIsActive() :void {
    this.isActive =!this.isActive;
  //hier fehlt was
  }
}
