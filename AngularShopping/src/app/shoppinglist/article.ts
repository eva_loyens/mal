export class Article {

    constructor(name: string, price:number, amount: number) {
        this._name = name;
        this._price = price;
        this._amount = amount;
        this._sum = amount * price;
        this._isSelected = false;
    }
    
    private _isSelected: boolean;
    public get isSelected(): boolean {
        return this._isSelected;
    }


    private _name: string;
    public get name(): string {
        return this._name;
    }

    private _price: number;
    public get price(): number {
        return this._price;
    }

    private _amount: number;
    public get amount(): number {
        return this._amount;
    }

    private _sum:number;
    public get sum(): number {
        return this._sum;
    }
}