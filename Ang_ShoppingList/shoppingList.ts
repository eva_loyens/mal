//interface is kein muss, kann man mit einer normalen class machen!!
export interface IItem {
    name: string,
    price: number,
    amount: number
}

//man muss nicht implements schreiben, es funktioniert trotzdem
export class Item 
/*implements IItem*/ {
    /*public name: string; 
    public price: number;
    public amount: number;*/

    //wenn man im constructor schon publi schreibt, spart man sich viel code, den man nicht haendisch schreiben muss
    constructor(public name: string, public price: number, public amount: number){
       /* this.name = name;
        this.price = price;
        this.amount = amount;*/
    }
}

