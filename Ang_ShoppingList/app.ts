import {Item} from "./shoppingList";
import {ShoppingList } from "./shoppingList";

var sl = new ShoppingList();

sl.addArticle("Milch", 1.20, 3);
sl.addArticle("Brot", 2.25, 4);
sl.addArticle("Butter", 1.65, 7);
sl.addArticle("Saft", 0.90, 2);
sl.addArticle("Eier", 0.42, 12);
sl.addArticle("Marm", 2.47, 2);

sl.printResult();