//man muss nicht implements schreiben, es funktioniert trotzdem
export class Item {
    /*public name: string;
    public price: number;
    public amount: number;*/
    //wenn man im constructor schon publi schreibt, spart man sich viel code, den man nicht haendisch schreiben muss
    constructor(name, price, amount) {
        this.name = name;
        this.price = price;
        this.amount = amount;
        /* this.name = name;
         this.price = price;
         this.amount = amount;*/
    }
}
