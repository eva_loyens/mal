import { Component, OnInit } from '@angular/core';
import { ShoppingList } from './shopping-list';
import { Article } from './article';

@Component({
  selector: 'shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.scss']
})
export class ShoppingListComponent implements OnInit {

  constructor() {
    this.fillShoppingList();
  }


  ngOnInit() {
  }

  onKeydown(event) {
    if (event.key === "Enter") {
      this.addToList();
    }
  }

  public shopping_list = new ShoppingList();
  public newArticleName = '';
  public newArticlePrice = '';
  public newArticleAmount = '';
  public showDetails = false;
  public selectedArticle = null;

  public fillShoppingList() {
    this.shopping_list.addArticle("Milch", 1.21, 3);
    this.shopping_list.addArticle("Eier", 0.38, 9);
    this.shopping_list.addArticle("Butter", 1.42, 4);
    this.shopping_list.addArticle("Mehl", 2.38, 2);
  }

  public addToList() {
    
    let price = parseFloat(this.newArticlePrice.replace(',', '.'))
    let amount = parseFloat(this.newArticleAmount.replace(',', '.'))

    if (this.newArticleName.trim() != '') {
      this.shopping_list.addArticle(this.newArticleName, (price ? price : 0), (amount ? amount : 1));
    }
    /*
    this.newArticleName = '';
    this.newArticlePrice = '';
    this.newArticleAmount = '';
    */
  }

  public removeFromList(article){
    this.shopping_list.removeArticle(article);
    this.showDetails = false;
    this.selectedArticle = null;
  }

  public showDetailsOfArticle(article: Article){

    if(this.selectedArticle == article)
    {
      this.showDetails = this.showDetails ? false: true;
      if(!this.showDetails)
      {
        this.selectedArticle = null;
      }
    }
    else{
      this.selectedArticle = article;
      this.showDetails = true;
    }
    
  }

}
