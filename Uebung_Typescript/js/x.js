class Animal {
    constructor(species) {
        this.species = species;
    }
    /**
     * species
     */
    getSpecies() {
        return this.name;
    }
    setSpecies(species) {
        this.species = species;
    }
    /**
     * name
     */
    getName() {
        return this.name;
    }
    setName(name) {
        this.name = name;
    }
    /**
     * age
     */
    getAge() {
        return this.age;
    }
    setAge(age) {
        this.age = age;
    }
}
var a = new Animal("Dog");
a["age"] = 7;
var count = 0;
for (var d in a) {
    if (count < 2) {
        console.log(a[d]);
        count++;
    }
}
console.log("\nAn animal: ");
console.log(a);
class Dog extends Animal {
    constructor() {
        super("dog");
    }
    getAge() {
        return this._age;
    }
    setAge(v) {
        this._age = v;
    }
    getName() {
        return this._name;
    }
    setName(v) {
        this._name = v;
    }
}
// var a1 = new Animal("Dog");
// import { objects } from "./";
var d1 = new Dog();
d1.setName("Azog");
console.log(d1);
