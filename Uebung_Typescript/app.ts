// var a1 = new Animal("Dog");
// import { objects } from "./";


var d1 = new Dog();
d1.setName("Azog");

console.log(d1);

interface cat {
    miauText : string;
}


var longHairCat = {
    name: "Milki",
    age: 3,
    miauText: "Miau"
}
doSomething(longHairCat);

function doSomething(myCat : cat) {
    console.log(myCat.miauText);
}

