class Animal {

    private species: string;
    private age: number;
    private name : string;

    constructor(species: string) {
        this.species = species;
 
    }

    /**
     * species
     */
    public getSpecies() {
        return this.name;
    }
    public setSpecies(species: string) {
        this.species = species;
    }

    /**
     * name
     */
    public getName() {
        return this.name;
    }
    public setName(name: string) {
        this.name = name;
    }

    /**
     * age
     */
    public getAge() {
        return this.age;
    }
    public setAge(age: number) {
        this.age = age;
    }
}

var a = new Animal("Dog");
a["age"] = 7;
var count = 0;
for(var d in a)
{ 
    if(count < 2)
    {
        console.log(a[d]);
        count++;
    }
}

console.log("\nAn animal: ")
console.log(a);