var person :number[] = [1, 2, 3, 4];

var animals: Array<string>;
// animals = [1, 2, 3];
animals = new Array();

animals.push("Dog");
animals.push("Cat");

console.log(person);
console.log(animals);


console.log("\nLoop array");
for(var i = 0; i < animals.length; i++){
    console.log(animals[i]);
}

console.log("\nForeach-Schleife with of");
for(var animal of animals){
    console.log(animal);
}

console.log("\nForeach-Schleife with in");
for(var animal in animals){
    console.log(animal);
}

