var result1 = add(1,2);
var result2 = add ("hello", "campus");
var result3 = add ("1", 1);

console.log(result1);
console.log(result2);
console.log(result3);

console.log(add(result2, result3));



function add(a, b)
{
    return a + b;
}

function addNumbers(a: number, b:number)
{
    return a + b;
}

var myfunc : Function = function(){
    console.log("test");
    return "test-result";
};

function test(callback: Function){
    var result = callback();
    console.log(result);
};


console.log("\nFunction in console.log\n", myfunc()); //only return test
console.log("\nJust function");
myfunc(); //returns test and test-result

console.log("\nfunction calling function")
test(myfunc);