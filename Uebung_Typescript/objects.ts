var dog: Object = {name: "Gustav", age:5};

console.log(dog);

console.log("\nForeach-Schleife with in");
for(var d in dog){
    console.log(d);
}
console.log("\n");
for(var d in dog){
    console.log(dog[d]);
}

