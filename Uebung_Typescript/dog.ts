class Dog extends Animal {
    constructor() 
    {
        super("dog");
    }

    
    private _age : number;
    public getAge() : number {
        return this._age;
    }
    public setAge(v : number) {
        this._age = v;
    }

    private _name : string;
    public getName() : string {
        return this._name;
    }
    public setName(v : string) {
        this._name = v;
    }
    
}