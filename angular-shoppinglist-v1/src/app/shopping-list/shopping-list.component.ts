import { Component, OnInit } from '@angular/core';
import { Article } from './article';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.scss']
})
export class ShoppingListComponent implements OnInit {

  private articles: Array<Article> = []

  public newName: string;
  public newPrice: number;
  public newAmount: number;
  public totalPrice: number = 0;

  constructor() { }

  ngOnInit() {
  }

  addArticle(): void {

    var article = new Article(this.newName, this.newPrice, this.newAmount);
    
    this.articles.push(article);
    this.totalPrice += article.sum;
  }
}
