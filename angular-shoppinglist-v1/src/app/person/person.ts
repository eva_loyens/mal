export class Person {
    constructor(name: string, age: number){
        this._name = name;
        this._age = age;
    }
    
    private _name : string;
    public get name() : string {
        return this._name;
    }

    private _age : number;
    public get age() : number {
        return this._age;
    }
}