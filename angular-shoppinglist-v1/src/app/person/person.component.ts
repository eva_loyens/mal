import { Component, OnInit } from '@angular/core';
import { Person } from "./person";

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {

  public message: string = "Hello Angluar";
  public showDiv: boolean = true;

  public persons: Array<Person> = [ 
    new Person("Hubert", 32),
    new Person("Susi", 21),
    new Person("Sorglos", 56)
   ];

   public newPerson: string;// = "Hello Hugo";

  constructor() { }

  ngOnInit() {

  }

  public updateMessage() : void {

    this.message = this.newPerson;
    //this.showDiv = !this.showDiv;
    //this.persons.push(new Person("Franz", 36));
  }

}
